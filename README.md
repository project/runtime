## Installation

```shell
### Uncomment for composer>=2.2
# composer config allow-plugins.symfony/runtime 1

composer config --json --merge extra.drupal-scaffold.allowed-packages '["drupal/runtime"]'
composer config extra.runtime.autoload_template vendor/drupal/runtime/assets/autoload_runtime.template
composer require drupal/runtime
```
